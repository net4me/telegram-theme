# Telegram client Theme

* [Theme mobile version](https://gitlab.com/net4me/telegram-theme/blob/master/net4me.attheme)
* [Theme linux desktop version](https://gitlab.com/net4me/telegram-theme/blob/master/net4me.tdesktop-theme)

# Telegram theme Screenshot

![](Telegram-Theme-Screenshot.png)

Goodluck! :+1:

